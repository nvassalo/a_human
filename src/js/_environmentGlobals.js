var serverPath;

if (process.env.NODE_ENV == 'development') {
    // update here for your local PHP server when developing
    serverPath = 'http://localhost:8888/'; /*'http://127.0.0.1:8080/server/';*/
} else {
    serverPath = 'https://ahumanbody.com/server/';
}


module.exports = {
    serverPath: serverPath
}