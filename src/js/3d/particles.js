import * as A from '../globals';
import { PointsMaterial, Points, BufferGeometry, BufferAttribute, AdditiveBlending, Color } from 'three';
  





export default class ParticleSystem {
  constructor(type, geometry, texture, scene) {
    this.type = type;
    
    this._material = new PointsMaterial({
      color: 0x333344,
      map: texture,
      blending: AdditiveBlending,
      //transparent: true,
      depthWrite: false,
      //depthTest: false,
      //opacity: 0.1
    });

    

    //this._geometry.addAttribute('position', new BufferAttribute(this._rawVertices, 3));
   


    if (type === 'contained') {
      this._material.size = 0.5 * A.dpi;
      this._geometry = geometry.clone();
      this.vertices = this._geometry.attributes.position;   
      this.mesh = new Points(this._geometry, this._material);
      // this.mesh.scale.set(1.15, 1.15, 1.15);
      this.mesh.rotation.y = -Math.PI/9;
      this.mesh.position.set(0, 1, -18);
      A.scene.add(this.mesh);

    } else if (type === 'full') {

      this._material.size = 2 * A.dpi;
      this._material.transparent = true;
      this._material.opacity = 0.5;
      this._material.color = new Color(0xcccccc);
      this._vertices = new Float32Array(120);  
      for (let i = 0; i < 120; i+=3) {
        let x = ((Math.random() * 2 - 1) * 30);
        let y = ((Math.random() * 2 - 1) * 20);
        let z = (Math.random()) * -20 + 20;
        this._vertices[i] = x;
        this._vertices[i+1] = y;
        this._vertices[i+2] = z;
      }
      this._geometry = new BufferGeometry();
      this._geometry.addAttribute('position', new BufferAttribute( this._vertices, 3 ) );
      this.vertices = this._geometry.attributes.position;
      this.mesh = new Points(this._geometry, this._material);
      this.mesh.position.set(0,0, -20);
      //this.mesh.scale.set(10, 10, 10)
      A.scene.add(this.mesh);
    }
    

  };

  update(time) {
    this._geometry.needsUpdate = true;
    this.vertices.needsUpdate = true;

   if(this.type === 'contained' ) {
      this.mesh.rotation.y += 0.001;
     for (let i = 0; i < this.vertices.count * 3; i += 3) {

       this.vertices.array[i] += 0.02 * (Math.sin((time * i * 0.000002)));
       this.vertices.array[i + 1] += 0.001 * (Math.cos((time * i * 0.00002)));
       this.vertices.array[i + 2] += 0.01 * (Math.sin((time * i * 0.000002)));
     } 
      
    } else if (this.type === 'full') {
      this.mesh.rotation.x -= 0.0005;
      this.mesh.rotation.y -=  0.0005;
     for (let i = 0; i < this.vertices.count * 3; i += 3) {

       this.vertices.array[i] += 0.005 * (Math.sin((time * i * 0.000005)));
       this.vertices.array[i + 1] += 0.005 * (Math.cos((time * i * 0.00005)));
       this.vertices.array[i + 2] += 0.005 * (Math.sin((time * i * 0.00005)));
     } 
    }
  
    
      
  }
}