import $ from 'jquery/src/jquery';
import * as A from '../globals.js';
import convert from 'color-convert';
import { mapRange } from '../util/util';
import { Vector3, TextureLoader, RepeatWrapping } from 'three';
import * as noUiSlider from 'nouislider/distribute/nouislider';


let inputs = $('input[type="range"]');
let colorInputs = $('input.color');
let bloomInput = $('input.bloom:visible');
let bumpInput = $('input.bump:visible');

// let twitchInput = $('input.twitch:visible');

let bumpMaps = [];
for (let i = 1; i < 8; i++) {
    bumpMaps.push(new TextureLoader().load(
        `${A.$paths.IMG_FOLDER}/bump0${i}.png`,
        texture => {
            texture.wrapS = texture.wrapT = RepeatWrapping;
        }
    ));
}


export function customizeControls (object) {
    let mobileColor = [A.r1, A.r2, A.r3];
    let mobileBloom = A.r4;
    let mobileBump = A.r5;
    
    inputs.on('mousedown mousemove', e => {
        // e.preventDefault();
        e.stopImmediatePropagation();
    })

    
    mobileColor.forEach((el, i) => {
        el.on('slide', (values, handle) => {
            
            let color = A.colorBar.update(values);
            A.heartParts[i].material.color.setRGB(...color);
        });
        el.on('start', () => $('body').css('overflow', 'hidden'))
        el.on('end', () => $('body').css('overflow', 'auto') )
    });

    mobileBloom.on('slide', (val, handle) => {
        let strength = mapRange(val, 0, 100, 0.0, 3.0);
        A.Bloompass.strength = Number(strength);
    });



    mobileBump.on('slide', (value, handle) => {
        let val = value - 1;
        if (A.heartParts.length > 0) {
            A.heartParts[0].material.bumpMap = bumpMaps[val];
            A.heartParts[1].material.bumpMap = bumpMaps[val];
            A.heartParts[2].material.bumpMap = bumpMaps[val];
        }
    })
    mobileColor.forEach((el, i) => {
        el.on('set', (values, handle) => {

            let color = A.colorBar.update(values);
            A.heartParts[i].material.color.setRGB(...color);
        });
        el.on('start', () => $('body').css('overflow', 'hidden'))
        el.on('end', () => $('body').css('overflow', 'auto'))
    });

    mobileBloom.on('set', (val, handle) => {
        let strength = mapRange(val, 0, 100, 0.0, 3.0);
        A.Bloompass.strength = Number(strength);
    });

    mobileBump.on('set', (value, handle) => {
        let val = value - 1;
        if (A.heartParts.length > 0) {
            A.heartParts[0].material.bumpMap = bumpMaps[val];
            A.heartParts[1].material.bumpMap = bumpMaps[val];
            A.heartParts[2].material.bumpMap = bumpMaps[val];
        }

    })


    

   /*  twitchInput.on('input', e => {
        e.stopImmediatePropagation();
        let val = $(e.target).val();
        let thresh = mapRange(val, 0, 100, 0, 10.0);
        if (val >= 1.0) {
            A.materialShaders[0].uniforms.time.value = thresh;
            A.materialShaders[1].uniforms.time.value = Math.max(0, (thresh *2) -thresh);
            A.materialShaders[2].uniforms.time.value = Math.max(thresh/2, 0);
        } else {
            A.heartParts[0].material.flatShading = true;
            A.heartParts[1].material.flatShading = true;
            A.heartParts[2].material.flatShading = true;
        }

        // A.sphereMesh.material.displacementScale = thresh
    }) */

}
