/**
 * Provides requestAnimationFrame in a cross browser way.
 * @author paulairish / http://paulirish.com/
 */

import {wW, wH, isMobile} from '../globals';
import {Vector3, Matrix4, Mesh} from 'three';

let targetRotationX = 0;
let targetRotationOnMouseDownX = 0;

let mouseX = 0;
let mouseXOnMouseDown = 0;


let windowHalfX = window.innerWidth / 2;

let slowingFactor = 0.05;

let isTouch = false;



export function init() {
    if ('ontouchstart' in window) {
        isTouch = true;
        // slowingFactor = 0.01;
    }
  
    isTouch ? document.addEventListener('touchstart', ondocumentMouseDown, false) : document.addEventListener('mousedown', ondocumentMouseDown, false);

}


function ondocumentMouseDown(event) {
    event.stopPropagation();

   
    isTouch ? document.addEventListener('touchmove', ondocumentMouseMove, false) : document.addEventListener('mousemove', ondocumentMouseMove, false);
    isTouch ? document.addEventListener('touchend', ondocumentMouseUp, false) : document.addEventListener('mouseup', ondocumentMouseUp, false);
    isTouch ? document.addEventListener('touchcancel', ondocumentMouseOut, false) : document.addEventListener('mouseout', ondocumentMouseOut, false);

    mouseXOnMouseDown = isTouch ? event.targetTouches[0].clientX - windowHalfX: event.clientX - windowHalfX;

    targetRotationOnMouseDownX = targetRotationX;

}

function ondocumentMouseMove(event) {

    mouseX = isTouch? event.targetTouches[0].clientX - windowHalfX : event.clientX - windowHalfX;

    
    targetRotationX = isTouch ? (mouseX - mouseXOnMouseDown) * 0.00075 : (mouseX - mouseXOnMouseDown) * 0.00025;

}

function ondocumentMouseUp(event) {

    document.removeEventListener('mousemove', ondocumentMouseMove, false);
    document.removeEventListener('touchmove', ondocumentMouseMove, false);
    document.removeEventListener('mouseup', ondocumentMouseUp, false);
    document.removeEventListener('touchend', ondocumentMouseUp, false);
    document.removeEventListener('mouseout', ondocumentMouseOut, false);
    document.removeEventListener('touchcancel', ondocumentMouseOut, false);
}

function ondocumentMouseOut(event) {

    document.removeEventListener('mousemove', ondocumentMouseMove, false);
    document.removeEventListener('touchmove', ondocumentMouseMove, false);
    document.removeEventListener('mouseup', ondocumentMouseUp, false);
    document.removeEventListener('touchend', ondocumentMouseUp, false);
    document.removeEventListener('mouseout', ondocumentMouseOut, false);
    document.removeEventListener('touchcancel', ondocumentMouseOut, false);
}


    
export function render(object) {
    //object.rotation.y += 0.02;

    
    if (targetRotationX > 0.002 || targetRotationX <= -0.002) {
        
        rotateAroundWorldAxis(object, new Vector3(0, 1, 0), targetRotationX);
    } else if (Math.sign(targetRotationX) == -1) {
        object.rotation.y -= 0.002
    } else {
        object.rotation.y += 0.002
    }

    targetRotationX = targetRotationX * (1 - slowingFactor);

}

function rotateAroundObjectAxis(object, axis, radians) {
    let rotationMatrix = new Matrix4();

    rotationMatrix.makeRotationAxis(axis.normalize(), radians);
    object.matrix.multiply(rotationMatrix);
    object.rotation.setFromRotationMatrix(object.matrix);

}




function rotateAroundWorldAxis(object, axis, radians) {

    let rotationMatrix = new Matrix4();

    object.traverse( child => {
        if (child instanceof Mesh) {
            rotationMatrix.makeRotationAxis(axis.normalize(), radians);
            rotationMatrix.multiply(child.matrix);                       // pre-multiply
            child.matrix = rotationMatrix;
            child.rotation.setFromRotationMatrix(child.matrix);
        }
    })

    
;
}