import { makeCall } from './util/order';
import {imageId, buyingOptions } from './globals';
import { serverPath } from './_environmentGlobals';


export function setPaypal (object) {

    const currency = object.currency || 'USD';
    const model = object.model;
    const amount = object.amount || 40.00;
    const color = object.color || 'white';


    paypal.Button.render({

        env: 'production', // sandbox | production
        style: {
            size: 'medium',
            color: 'silver',
            shape: 'rect',
            label: 'checkout',
            tagline: 'false',
            height: 45
        },

        // PayPal Client IDs - replace with your own
        // Create a PayPal app: https://developer.paypal.com/developer/applications/create
        client: {
            sandbox: 'AZ_L_XGaePAJfT8RU3fW1wM13OeAedzMaI53Y7dy3P5nH_ONsOi3uIbOYfHP35Vti5idRbQ2lfsF1om8',
            production: 'ATacMbVwv9saabG8VYr9z2_WYHSlH85s0iwox1WC-ZH0f4_qs_w4uo3UIBSIwB6oV5m5mp6fhTsGY3FU'
        },

        // Show the buyer a 'Pay Now' button in the checkout flow
        commit: true,

        // payment() is called when the button is clicked
        payment: function (data, actions) {
            

            // Make a call to the REST api to create the payment
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: amount, currency }
                        }
                    ]
                }
            });
        },

        // onAuthorize() is called when the buyer approves the payment
        onAuthorize: function (data, actions) {
            // Make a call to the REST api to execute the payment
            let newData = data;
            return actions.payment.execute().then(function (data) {
              //  console.log("paypal data", data.payer.payer_info);
                let info = data.payer.payer_info.shipping_address;
                let line2 = info.line2 !== undefined ? `, ${info.line2}` : '';
                let imageUrl = `${serverPath}images/${buyingOptions.color}_${imageId}.png`; 
                makeCall(`${info.line1}${line2}`, info.city, info.recipient_name, info.country_code, info.state, info.postal_code, data.payer.payer_info.email, imageUrl, buyingOptions.model);
                $('body').append(`
                    <div class="print-loader">
                        <div class="frame frame_1">Your heart is on its way!</div>
                    </div>
                `);
                setTimeout(()=> {
                    $('.print-loader').fadeOut(200, 'swing', ()=> {
                        $('.print-loader').remove();
                    })      
                }, 2500);
                
            });
        }

    }, '.pay');
}