import * as T from 'three';
import $ from 'jquery/src/jquery';
import * as dat from 'dat.gui';
import ScrollMagic from 'ScrollMagic';
import {TweenMax} from 'gsap';
import ScrollToPlugin from "gsap/ScrollToPlugin";
import 'debug.addIndicators';
import 'animation.gsap';
import { OBJLoader } from 'three/examples/js/loaders/OBJLoader';
import { OrbitControls } from 'three/examples/js/controls/OrbitControls';
import { UnrealBloomPass } from 'three/examples/js/postprocessing/UnrealBloomPass';
import { RenderPass } from 'three/examples/js/postprocessing/RenderPass';
import { ShaderPass } from 'three/examples/js/postprocessing/ShaderPass';
import { LuminosityHighPassShader } from 'three/examples/js/shaders/LuminosityHighPassShader';
import { FXAAShader } from 'three/examples/js/shaders/FXAAShader';
import { CopyShader } from 'three/examples/js/shaders/CopyShader';
import { EffectComposer } from 'three/examples/js/postprocessing/EffectComposer';
import { Reflector } from 'three/examples/js/objects/Reflector';
import { SimplifyModifier } from 'three/examples/js/modifiers/SimplifyModifier';
import * as A from './globals';
import resizeUpdate from './resizeUpdate';
import ParticleSystem from './3d/particles';
import mobile from './mobileMenu';
import {rangeSlider, setMobileRange } from './mobileRange';
import {customizeControls} from './3d/customizeModel';
import colorBar from './colorBar';
import * as Rotator from './3d/rotation';
import { getArgs } from './util/util';
import { setPaypal } from './pay';
import { startCart } from './util/checkout';
import { setCustomSelect } from './customSelect';
import { serverPath } from './_environmentGlobals';
import { makeCall } from './util/order';
import { model } from './util/setShirtModel';
import * as noUiSlider from 'nouislider/distribute/nouislider';



function addGui(heart) {
  let params = {
    color1: 0xc60002,
    color2: 0xc60002,
    color3: 0xc60002,
    bloomStrength: 1.5,
    textures: "1",
    distort: 0.0
  };
  A.gui = new dat.GUI();
  A.gui.open();

  A.gui.addColor( params, 'color1').onChange( value => {

    A.heartParts[0].material.color.set(value);
    

    
  });
  A.gui.addColor(params, 'color2').onChange(value => {
    A.heartParts[1].material.color.set(value);

  });
  A.gui.addColor(params, 'color3').onChange(value => {

    A.heartParts[2].material.color.set(value);

  });

  A.gui.add( params, 'bloomStrength', 0.0, 3.0 ).onChange( function ( value ) {

    A.Bloompass.strength = Number( value );

  } );

  A.gui.add(params, 'textures', ['1', '2', '3', '4', '5', '6', '7'] ).onChange( function ( value ) {

    new T.TextureLoader().load(
      `${A.$paths.IMG_FOLDER}/bump0${value}.png`,
      texture => {
        texture.wrapS = texture.wrapT = T.RepeatWrapping;
        A.heartParts[0].material.bumpMap = texture;
        A.heartParts[1].material.bumpMap = texture;
        A.heartParts[2].material.bumpMap = texture;
      }

    );

  } );

  A.gui.add( params, 'distort', 0.0, 100.0 ).onChange( function ( value ) {

    A.materialShaders[0].uniforms.time.value = value;
    A.materialShaders[1].uniforms.time.value = Math.max(0, (value * 2) - value);
    A.materialShaders[2].uniforms.time.value = Math.max(value / 2, 0);

  } );

}

let isOrbit = false;

function toggleOrbit(controls) {
  $('canvas').on('mousedown', () => {
    isOrbit ? isOrbit = false : isOrbit = true;
    isOrbit ? A.controls.enabled = true : A.controls.enabled = false
  })
}


function setup() {
  mobile();
  A.play = true;
  A.buyingOptions = {
    amount: 40.00,
    color: 'white',
    model: model('S', 'white')
  }

  A.dpi = window.devicePixelRatio;
   

  let heartRefract = new T.CubeTextureLoader()
    .setPath(A.$paths.IMG_FOLDER + '/')
    .load([
        '01.jpg',
        '01.jpg',
        '01.jpg',
        '01.jpg',
        '01.jpg',
        '01.jpg'
    ]);
  A.wW = window.innerWidth;
  A.wH = window.innerHeight;

  
  A.scene = new T.Scene();

  let displacementMap;
  let normalMap;

  new T.TextureLoader().load(
    `${A.$paths.IMG_FOLDER}/bump04.png`,
    texture => {
      texture.wrapS = texture.wrapT = T.RepeatWrapping;
      A.bumpMap = texture
    }
  );
    new OBJLoader().load(
      `${A.$paths.OBJ_FOLDER}/heart_particle.obj`,
      obj => 
      obj.traverse( child => {
        if ( child instanceof T.Mesh ) {
          A.particleGeometry = child.geometry;
          new T.TextureLoader().load(
            `${A.$paths.IMG_FOLDER}/heartParticle.png`,
            texture => {
              if (!A.isMobile) {
                A.particle = new ParticleSystem('contained', A.particleGeometry, texture, A.scene);
              }
            }
          );
        }
      })
    )

   

    const loader = new OBJLoader().load(
      `${A.$paths.OBJ_FOLDER}/heart.obj`,
      (object) => {
        let i = 0;
        object.traverse( child => {
        
          if (child instanceof T.Mesh) {

      
            child.material = new T.MeshPhysicalMaterial({
              color: 0xc60002,
              roughness: 0.5,
              metalness: 0.1,
              reflectivity: 0.9,
              clearCoat: 1,
              bumpMap: A.bumpMap,
              bumpScale: 0.4,
              flatShading: true,
              envMap: heartRefract,
              envMapIntensity: 4,
              // normalMap,
              // normalScale: new T.Vector3(0,0),
              //  displacementMap,
              //  displacementScale: 0.1
              //transparent: true,
              //premultipliedAlpha: true,
              //opacity: 0.85,
            // side: T.DoubleSide

            })

            child.material.onBeforeCompile = (shader) => {
              // console.log( shader )
              shader.uniforms.time = { value: 0 };
              shader.vertexShader = 'uniform float time;\n' + shader.vertexShader;
              shader.vertexShader = shader.vertexShader.replace(
                '#include <begin_vertex>',
                [
                  'mat3 m;',
                  'float theta = sin( time + position.x ) / 4.0;',
                  'float c = cos( theta );',
                  'float s = sin( theta );',
                  'if (time >= 1.0) {',
                  'm = mat3( c, 0, s, 0, 1, 0, -s, 0, c );',
                  '} else {',
                  'm = mat3(1);',
                  '}',
                  'vec3 transformed = vec3( position ) * m;',
                  '#ifndef FLAT_SHADED',
                  'vNormal = vNormal * m;',
                  '#endif'
                ].join('\n')
              );
              A.materialShaders.push(shader);
            };

            A.heartParts.push(child); 
            
            let geometry = child.geometry;
            geometry.attributes.uv2 = geometry.attributes.uv;


            
            //A.scene.add(arrowHelper);
          //child.geometry.center();
            
          // scene.add(line);         
          
          }
    
        });
        
        new T.TextureLoader().load(
          `${A.$paths.IMG_FOLDER}/dustParticle.png`,
          texture => {
            if (!A.isMobile) {
              A.fullParticle = new ParticleSystem('full', null, texture, A.scene);
            }
            A.updateCubeCamera = true;
          }
        );
        i += 1;
        A.heart = object;




        A.heart.position.set(4, 0, -25);
        //A.particle.mesh.position.set(A.heart.position);




        A.heart.scale.set(1.1, 1.1, 1.1);
        A.scene.add(A.heart);
        /*A .heart.traverse( child => {
          child.visible = false
        }) */
        //A.scene.add(A.sphereMesh);




        //A.scene.add(hearthelp);
        A.camera.lookAt(A.heart.position);
        //A.scene.add(new T.AxesHelper());
        // heart.scale.set(0.9, 0.9, 0.9);
        //heart.rotateZ(Math.PI/9);
        //light.target = heart;
        //scene.add(light.target);  
        A.cubeCamera = new T.CubeCamera(1, 200, 1024);
        A.scene.add(A.cubeCamera);
        // cubeCamera.position.copy(heart.position);
        A.cubeCamera.position.set(0, -32, 6);
        //cubeCamera.rotation.x = -Math.PI;
        // cubeCamera.scale.set(heart.scale);


        A.planeMat = new T.MeshPhongMaterial({
          color: 0x000000,
          envMap: null,
          refractionRatio: 0.9,
          blending: T.AdditiveBlending,
          shininess: 0,
          specular: 0xff0000,
          emissive: 0xffffff,
          alphaTest: 0,
          opacity: 0.25,


          //lights: false
          flatShading: true,
          transparent: true,
          fog: false
        });

        let planeGeo = new T.PlaneBufferGeometry(2000, 2000);
        //planeGeo.rotateX(-Math.PI / 2);


        A.planeBottom = new T.Mesh(planeGeo, A.planeMat);
        A.planeBottom.rotation.x = -Math.PI / 2;

        A.planeMat.envMap = A.cubeCamera.renderTarget.texture;
        A.cubeCamera.renderTarget.texture.mapping = T.UVMapping;
        A.cubeCamera.renderTarget.texture.minFilter = T.LinearMipMapLinearFilter;

        A.scene.add(A.planeBottom);

        A.planeBottom.position.y = -12;
        A.planeBottom.position.z = 0; 

        //end traverse
        $('body').removeClass('loading');
        setTimeout(() => $('.loader-overlay').remove(), 1000);
        //addGui(A.heart);
        customizeControls(A.heart);



        
        
      },
      //end load
      (xhr) => {
        console.log( `${xhr,  ( xhr.loaded / xhr.total * 100 )}% loaded` );
      },
      (err) => {
        console.log( `ERROR: ${err}` );
      }
    );
  let pointLight1 = new T.SpotLight( {
    color: 0x990000,
    distance: 200,
    decay: 1,
    angle: 1,
    penumbra: 0.2
  });
  pointLight1.position.set( 0, 0, -2 );
  pointLight1.rotation.set (Math.PI/9, 0, 0);
  pointLight1.castShadow = true;
  A.scene.add( pointLight1 );
  let pointLight2 = new T.SpotLight( {
    color: 0x000055,
    distance: 50,
    decay: 1,
    penumbra: 0.2,
    angle: 1
  });
  pointLight2.position.set( -150, 0, 0 );
  A.scene.add( pointLight2 );
  let pointLight3 = new T.SpotLight( {
    color: 0x000055,
    distance: 50,
    decay: 1,
    penumbra: 0.2,
    angle: 1
  });
  pointLight3.position.set( 0, -10, 10 );
  A.scene.add( pointLight3 );
  let pointLight4 = new T.SpotLight( {
    color: 0x000055,
    distance: 50,
    decay: 1,
    penumbra: 0.2,
    angle: 1
  });
  pointLight4.position.set( 0, 0, -40 );
  A.scene.add( pointLight4);

  // light = new T.DirectionalLight(0xffffff, 1);
  // scene.add(light);

  A.bgTexture = new T.TextureLoader().load('/assets/imgs/bg.jpg');

  A.scene.background = A.bgTexture;

 

  let canvas = document.getElementById('stage')
  A.renderer = new T.WebGLRenderer({ antialias: true, canvas, fog: false, alpha: true });
  A.renderer.setSize(A.wW, A.wH);
  A.camera = new T.PerspectiveCamera( 80, A.wW / A.wH, 0.1, 4000 );
  // camera = new T.OrthographicCamera(wW / - 2, wW / 2, wH / 2, wH / - 2, 1, 500);
  A.scene.fog = new T.Fog(0xA297FF, 21, 50);
  A.camera.position.set( 0, 0, 5.067444072914755);





 

  //A.controls = new T.OrbitControls(A.camera);
  // A.controls.enabled = false;
   //A.camera.lookAt(A.scene.position);
  // A.controls.update();
  // toggleOrbit(A.controls);


  A.composer = new T.EffectComposer(A.renderer);
  A.renderScene = new T.RenderPass(A.scene, A.camera);
  A.FXAAPass = new T.ShaderPass(T.FXAAShader);
  A.FXAAPass.uniforms.resolution.value.set(1/A.wW , 1/A.wH );

  A.Bloompass = new T.UnrealBloomPass(new T.Vector2( A.wW , A.wH ), 1.5, 0.5, 0.5);
  A.Bloompass.renderToScreen = true;
  A.composer.setSize(A.wW * A.dpi, A.wH * A.dpi);
  // composer.addPass(backgroundRenderScene);
  A.composer.addPass(A.renderScene);
  if (A.dpi < 2) {
    A.composer.addPass(A.FXAAPass);
  }
  A.composer.addPass(A.Bloompass);

  // $('.btn-text').on('click', e => {
  //   makeCall('Rua', 'NY', 'nelson', 'PT', '', '1050-179', 'vassalo.nelson@gmail.com', 'https://ahumanbody.com/test/server/images/black_heart_5b7f1b4cc24d3.png')
  // });

  A.fromHome = false;

  if (window.location.hash === '#faq') {
    A.fromHome = true;
    setTimeout(openFAQModal, 2000);
  }

  $('.faq-btn').on('click', e => {
    e.preventDefault();
    A.fromHome = false;
    openFAQModal();
  })


  function openFAQModal() {

    let faq = $('.privacy-policy');
    let closeBtn = $('.faq.close');
    let prevScene;

    

    
    if (window.location.hash && window.location.hash != '#') {
      prevScene = window.location.hash;
    }
    
    history.replaceState(null, null, `#faq`);
    $('body').css('overflow', 'hidden');
    faq.addClass('open');
    closeBtn.addClass('open');
    closeBtn.on('click', e => {
      e.preventDefault();
      faq.removeClass('open');
      closeBtn.removeClass('open');
      $('body').css('overflow', 'auto');
      if (A.fromHome == true) {
        window.scrollTo(0,0);
        history.pushState(null, null, window.location.href.split('#')[0]);
      } else {
        history.replaceState(null, null, prevScene);
      }
    })
    //fromHome = false;
  }

  

  

  rangeSlider();
  setMobileRange();
  setCustomSelect();
  setScrollMagic();
  smoothScroll();
  animate();
  Rotator.init();
  resizeUpdate(A.camera, A.renderer);
  
  startCart();
  A.colorBar = new colorBar();
  A.colorBar.setup();

  $('footer .mobile-col input').on('blur', e=> {
    $(window).scrollTop(0);
  }).on('focus', e => {
    
    $('.mobile-col').stop().animate({
      'opacity': '1'
    }, 200);
  });
  $('.shop').on('touchmove', e=>  {
    e.preventDefault();
    e.stopPropagation();
  });

  $('input[type=email]').on('focus', e=> {
    A.controller.enabled(false);
    let inputs = $('input');
      
  })
  .on('blur focusout', e=> {
    e.preventDefault();
    e.stopPropagation();
    A.controller.enabled(true);
    A.controller.update(true);
  });
  let inputs = jQuery("input");
    $(document).on('touchstart', 'body', function (evt) {
      var targetTouches = evt.targetTouches;
      if (!inputs.is(targetTouches)) {
        document.activeElement.blur();
      }
    });
  
  let viewportHeight = $('#heart').outerHeight();
  $('#heart').css({ height: viewportHeight });
    

  setPaypal(A.buyingOptions);



  //rangeAccordion();
 

  


  window.scene = A.scene;

}


function smoothScroll() {
  $('header nav a:not(.faq-btn)').on('click', e => {
    e.preventDefault();
    let id = e.target.getAttribute('href');
    if($(id).length > 0) {
      A.controller.scrollTo(id)
    }
  })
  $('header h1 a').on('touchstart click', e=> {
    e.preventDefault();
    A.controller.scrollTo(0);
  })
}

const scrollScenes = [];
const scenes = Array.prototype.slice.call(document.querySelectorAll('section:not(#heart):not(.privacy-policy)'));


function setScrollMagic() {

  A.controller = new ScrollMagic.Controller();


  let oldPos,
    h1 = document.querySelectorAll('h1')[0],
    nav = $('header nav'),
    links = $('header nav li a');  

    const setDuration = (scene) => (scene.id === 'merchandise' && A.isMobile) ? '150%' : '100%';

  scenes.forEach((sceneEl, i) => {
    let final;
    if( sceneEl.id === 'contact') {
      final = '#contact'
    }


    let blocks = $(sceneEl).find('.col');
    let sliders = $('#merchandise .mobile-sliders');


    let _scene = new ScrollMagic.Scene({
      triggerElement: sceneEl,
      reverse: true,
      duration: setDuration(sceneEl),
      triggerHook: 'onCenter'
    })
      .setClassToggle(links[i], 'active')
      //.setPin(scenes[i], {pushFollowers: true})
      //.addIndicators({name: sceneEl.id })
      .on('enter', e => {
        let id = sceneEl.id;
        //window.location.hash = `#${id}`;
        history.replaceState(null, null, `#${id}`);
        if (!A.isMobile) TweenMax.to(blocks, 1, { opacity: 1, ease: Power4.easeOut}, 0.5)  
    
      })
      .on('progress', e => {

        if (!A.isMobile && e.progress > 0.85 && e.scrollDirection === 'FORWARD') {
          TweenMax.to(blocks, 1, { opacity: 0, ease: Power4.easeOut }, 0.5)
        }
        if (!A.isMobile && e.progress > 0.85 && e.scrollDirection === 'REVERSE') {
          TweenMax.to(blocks, 1, { opacity: 1, ease: Power4.easeOut }, 0.5)
        }
        if (sceneEl.id === 'merchandise' && A.isMobile) {
          if (e.progress > 0.15 && e.progress < 0.9 && e.scrollDirection == 'FORWARD') {
            TweenMax.to(sliders, 1, {opacity: 1, ease: Power4.easeOut});
          }
          else if (e.progress <= 0.15 && e.scrollDirection == 'REVERSE') {
            TweenMax.to(sliders, 1, { opacity: 0, ease: Power4.easeOut});
          }
          else if (e.progress >= 0.85 && e.scrollDirection == 'FORWARD') {
            TweenMax.to(sliders, 1, { opacity: 0, ease: Power4.easeOut});
          }
          else if (e.progress < 0.9 && e.scrollDirection == 'REVERSE') {
            TweenMax.to(sliders, 1, { opacity: 1, ease: Power4.easeOut });
          }
        }
        if (sceneEl.id === 'merchandise' && !A.slidersShow) {
           
          if (e.progress > 0.35 && e.scrollDirection === 'FORWARD') {
            let inputsVal;
            if (A.isMobile) {
              inputsVal = Array.prototype.slice.call(document.querySelectorAll('.mobile-sliders .group > div'));
            } else {
              inputsVal = Array.prototype.slice.call(document.querySelectorAll('.sliders .group > div'));
            }
      
            let obj = [
              { val: 0 },
              { val: 0 },
              { val: 0 },
              { val: 0 },
              { val: 0 }
            ];
              inputsVal.forEach((el, i) => {
                const max = () => {
                  if (i < 3) {
                    return 500
                  } else if (i === 3) {
                    return 50
                  } else {
                    return 4
                  }
                }
              
              TweenMax.to(obj[i],  2, { val: max(), ease: Power4.easeOut, delay: 0.35 * i, onUpdate: updateThis });
              function updateThis() {
                el.noUiSlider.set(obj[i].val);
              }
            })
            A.slidersShow = true;
            }
          }
      })

      .addTo(A.controller);

      scrollScenes.push(_scene);


    if (i === 0) {
      _scene.on('enter', e => {
        if (e.scrollDirection === 'FORWARD') {
          function stagger() {
            TweenMax.staggerTo(links, 0.2, { x: 0, opacity: 1, ease: Power4.easeOut }, 0.1);
          }
          stagger();
        }
      })
      .on('leave', e => {
        if (e.scrollDirection === 'REVERSE') {
          function staggerReverse() {
            TweenMax.staggerTo(links, 0.2, { x: '-30px', opacity: 0, ease: Power4.easeOut, yoyo: true }, 0.1);
          }
          staggerReverse()
        }
      })
      
    }

    scrollScenes.push(_scene);
  })
  let h1Scene;
  if (A.isMobile) {
    h1Scene = TweenMax.to(h1, 1, { y: 0, x: '-50%', top: 1, height: 25, ease: Power4.easeOut, yoyo: true });
  } else {
    h1Scene = TweenMax.to(h1, 1, { y: 0, x: '-50%', top: 26, height: 38, ease: Power4.easeOut, yoyo: true });
  }


if (A.isMobile) {
  let mobileInitScene = new ScrollMagic.Scene({
    triggerHook: 0,
    duration: '60%',
    offset: 0
  })
  .on('enter', e => {
    if (e.scrollDirection === 'REVERSE') {
      function transp() {
        TweenMax.to($('footer'), 1, { opacity: 1, ease: Power4.easeOut, onComplete: transp })

      }
      TweenMax.set($('footer'), { visibility: 'visible', onComplete: transp })
    }

  })
  .on('leave', e => {
      if (e.scrollDirection === 'FORWARD') {
        function opac() {
        TweenMax.set($('footer'), { visibility: 'hidden' })
      }
      TweenMax.to($('footer'), 1, { opacity: 0, ease: Power4.easeOut, onComplete: opac })
    }
  })
  .addTo(A.controller);
}


  let initScene = new ScrollMagic.Scene({
    triggerHook: 0.5,
    offset: 20,
    duration: "46.5%"
  })
    .setTween(h1Scene)
    //.addIndicators({name: "initial"})
    .addTo(A.controller);


  initScene.on('enter', e => {
    history.pushState(null, null, window.location.href.split('#')[0]);
    
    function opac() {
      if (!A.isMobile) TweenMax.set($('footer'), { visibility: 'hidden' }); 
    }
    if(!A.isMobile) TweenMax.to($('footer'), 1, { opacity: 0, ease: Power4.easeOut, onComplete: opac });

    // TweenMax.to($('#arrow'), 1, {opacity: 0, ease:  Power4.easeOut});
  })

  initScene.on('leave', e => {
    
    function transp() {
      if (!A.isMobile) TweenMax.to($('footer'), 1, { opacity: 1, ease: Power4.easeOut, onComplete: transp });

    }
    if (e.scrollDirection === 'REVERSE') {
      if(!A.isMobile) TweenMax.set($('footer'), { visibility: 'visible', onComplete: transp });
      //TweenMax.to($('#arrow'), 1, {opacity: 1, ease:  Power4.easeOut});

    }
  })

  A.controller.scrollTo( newPos => {
    TweenMax.to(window, 2, { scrollTo: { y: newPos, autoKill: false },  delay: 0.1, ease:  Power4.easeOut });
  })
}


let time = 0;


function animate() {
    time ++;
    //A.controls.update();
    if(A.particle) {
        A.particle.update(time);
    }
    if (A.fullParticle) {
      A.fullParticle.update(time);
    }
    
    if (A.updateCubeCamera) {

      A.scene.background = null;
      A.planeBottom.visible = false;
      if (!A.isMobile) {
        A.particle.mesh.visible = false;
        A.fullParticle.mesh.visible = false;
      }
      A.cubeCamera.update(A.renderer, A.scene);
      A.planeBottom.visible = true;
      if (!A.isMobile) {
        A.particle.mesh.visible = true;
        A.fullParticle.mesh.visible = true;
      }
      A.scene.background = A.bgTexture

    }
    if (A.heart && !A.isMobile) {
      //A.heart.rotation.y += 0.002;
      Rotator.render(A.heart);
    } else if (A.heart) {
      A.heart.rotation.y += 0.002;
    }
    
    A.composer.render();
  
    // A.renderer.render();
    if(A.play) {
      window.requestAnimationFrame(animate);
    }
    
}

$(document).ready( function() {
  setup();
});