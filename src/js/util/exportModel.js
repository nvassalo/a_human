import * as A from '../globals';
import * as T from 'three';
import resize from '../resizeUpdate';
import { serverPath } from '../_environmentGlobals';
import { setCustomSelect } from '../customSelect';

let exportRenderer;

let oldRotation;
function resetDefaults() {
    // A.renderer.setSize(3000, 3000);
    A.play = false;

    A.renderer.setSize(3150, 3150);
 
    oldRotation = {
        heart: A.heart.rotation.y,
        heartParts: [
            A.heartParts[0].rotation.y,
            A.heartParts[1].rotation.y,
            A.heartParts[2].rotation.y,
        ],
    };
    A.camera.aspect = 1;
    A.camera.updateProjectionMatrix();
    A.renderer.setClearAlpha(0);
    A.renderer.setClearColor(0xffffff, 0.0);
    A.scene.background = null;
    A.planeBottom.visible = false;
    if(!A.isMobile) {
        A.particle.mesh.visible = false;
        A.fullParticle.mesh.visible = false;
    }

    //w.document.body.style.backgroundColor = "red";
    var img = new Image();
    
    // Without 'preserveDrawingBuffer' set to true, we must render now
    A.heart.rotation.y = 1;
    A.heartParts[0].rotation.set(0, 0, 0);
    A.heartParts[1].rotation.set(0, 0, 0);
    A.heartParts[2].rotation.set(0, 0, 0);
    A.heartParts[0].updateMatrix();
    A.heartParts[1].updateMatrix();
    A.heartParts[2].updateMatrix();


    A.renderer.render(A.scene, A.camera, null, false);
    img.src = A.renderer.domElement.toDataURL();

    
    setToPrevious(img);   
}


function base64ToBlob(base64, mime) {
    mime = mime || '';
    var sliceSize = 1024;
    var byteChars = window.atob(base64);
    var byteArrays = [];

    for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
        var slice = byteChars.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, { type: mime });
}



function sendOver(img, color) {
    var block = img.split(";");
    let contentType = block[0].split(":")[1];// In this case "image/gif"
    let realData = block[1].split(",")[1];
    let blob = base64ToBlob(realData, 'image/png'); 
    let formData = new FormData();
    formData.append('img', blob);

    $.ajax({
        type: "POST",
        contentType: false,
        processData: false,
        cache: false,
        //crossDomain: true,
        //dataType: 'html',
        url: serverPath + 'images.php',
        data: formData,
        success:function (data) {
            $('.heart-preview img').attr('src', `${serverPath}images/thumb_${ A.buyingOptions.color }_${data}.png`).css('visibility', 'visible');
            A.imageId = data;
            $('.print-loader').remove();
        }
    }).fail(function(jqXHR, textStatus, error){
        if ($('.print-loader').length > 0) $('.print-loader').remove();
        $('body').append(`
                    <div class="print-loader error">
                        <div class="frame frame_1">Something went wrong.<br> Please try resubmitting your design or contact us!</div>
                    </div>
                `);
        setTimeout(() => {
            $('.print-loader').fadeOut(200, 'swing', () => {
                $('.print-loader').remove();
                $('#merchandise').removeClass('buying');
            })
        }, 2500);
        console.info("Error!", error);
        console.info("code:", textStatus);
        console.info("object:", jqXHR);
    })
}

function setToPrevious(img, color) {
    A.camera.aspect = window.innerWidth / window.innerHeight;
    A.camera.updateProjectionMatrix();

    A.renderer.setSize(window.innerWidth, window.innerHeight);
    A.heart.rotation.y = oldRotation.heart;
    A.heartParts[0].rotation.y = oldRotation.heartParts[0];
    A.heartParts[1].rotation.y = oldRotation.heartParts[1];
    A.heartParts[2].rotation.y = oldRotation.heartParts[2];
    A.planeBottom.visible = true;
    if (!A.isMobile) {
        A.particle.mesh.visible = true;
        A.fullParticle.mesh.visible = true;
    }
    A.play = true;
    setTimeout(()=> {
        sendOver(img.src, color);  
    }, 400)
}

export function setPrint() {
    //A.play = false;
    resetDefaults();
    //A.renderer.setSize(4000, 4000);
}