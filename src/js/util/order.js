import {  getArgs } from './util';
import { serverPath } from '../_environmentGlobals';

export function makeCall(address, city, name, country, state, zip, email, image, model) {
    let args = Array.from(arguments);
    
    let _data = {
        "address": address,
        "city": city,
        "name": name,
        "country": country,
        "state": state,
        "zip": zip,
        "email": email,
        "image": image,
        "model": model + ''
    };
    let data = JSON.stringify(_data);

    // $.get('http://127.0.0.1:8080/server/orders.php?' + _data, e => {
    //     console.log(e);
    // });
    $.ajax({
        url: serverPath + 'orders.php',
        type: 'POST',
        crossDomain: true,
        data: data,
        processData: false,

        success: (data) => {
           // console.log("final data: ", data);
        }
    }).fail(function(data){
        if ($('.print-loader').length > 0) $('.print-loader').remove();
        $('body').append(`
                    <div class="print-loader error">
                        <div class="frame frame_1">Something went wrong.<br> Please try resubmiting your design or contact us!</div>
                    </div>
                `);
        setTimeout(() => {
            $('.print-loader').fadeOut(200, 'swing', () => {
                $('.print-loader').remove();
                $('#merchandise').removeClass('buying');
            })
        }, 2500);
        console.error("Error: " + data);
    });
}