export function model(size, color) {
    if (color === 'white') {
        if (size === 'S') {
            return 4011
        } else if (size === 'M') {
            return 4012
        } else if (size === 'L') {
            return 4013
        } else if (size === 'XL') {
            return 4014
        } else if (size === 'XXL') {
            return 4015
        } else if (size === 'XXXL') {
            return 5308
        }
    }
    else if (color === 'black') {
        if (size === 'S') {
            return 4016
        } else if (size === 'M') {
            return 4017
        } else if (size === 'L') {
            return 4018
        } else if (size === 'XL') {
            return 4019
        } else if (size === 'XXL') {
            return 4020
        } else if (size === 'XXXL') {
            return 5295
        }
    }

}