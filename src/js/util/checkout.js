import * as A from '../globals';
import {setPrint} from '../util/exportModel';

export function startCart() {
    setEvents();
}

function setEvents() {
    $('.return').on('click', e=> {
        e.preventDefault();
        $('#merchandise').removeClass('buying');
        $('.shop').removeClass('visible');

    })
    $('.buy-btn').on('mousedown click', e => {
        e.preventDefault();
       $('.shop').addClass('visible');
        setTimeout(() => $('#merchandise').addClass('buying'), 100); 
        A.controller.scrollTo('#merchandise');    
        $('body').append(
            `<div class="print-loader">
                <div class="frame frame_1">Creating your heart</div>
                <div class="frame frame_2">Wait a beat</div>
                <div class="frame frame_3">Almost there</div>
        </div>`
        );
    
        setTimeout(e => {
            setPrint();
        }, 500);    

    })
}