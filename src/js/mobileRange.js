import $ from 'jquery/src/jquery';
import * as A from './globals.js';
import * as noUiSlider from 'nouislider/distribute/nouislider';


export function rangeSlider() {
    function setValue(el) {
        var val = (((el.val() - $(el).attr('min')) / ($(el).attr('max') - $(el).attr('min')) * 100) + '%');
        el.css('background', 'linear-gradient(to right, #F2310B ' + val + ', #ffe0e0 ' + val + ')');
    }
    setValue($('input[type="range"]'));
    $('input[type="range"]').on('input change', function (e) {
        setValue($(e.target));
    });
}

let accordion = $('.group fieldset');
let btn = $('.controls a');

const setOrientation = () => A.isMobile ? 'vertical' : 'horizontal';
export function setMobileRange() {
    if (A.isMobile) $('.mobile-sliders .group, .sliders .group').on('touchstart', e=> { e.preventDefault(); e.stopPropagation()});
    const sliderEl = () => A.isMobile ? document.querySelector('.mobile-sliders') : document.querySelector('.sliders');
    let r1 = sliderEl().querySelector('.r-1');
    let r2 = sliderEl().querySelector('.r-2');
    let r3 = sliderEl().querySelector('.r-3');
    let r4 = sliderEl().querySelector('.r-4');
    let r5 = sliderEl().querySelector('.r-5');
    A.r1 = noUiSlider.create(r1, {
        start: 0,
        connect: [true, false],
        orientation: setOrientation(),
        range: {
            'min': 0,
            'max': 1000
        }
    })
    A.r2 = noUiSlider.create(r2, {
        start: 0,
        connect: [true, false],
        orientation: setOrientation(),
        range: {
            'min': 0,
            'max': 1000
        }
    })
    A.r3 = noUiSlider.create(r3, {
        start: 0,
        connect: [true, false],
        orientation: setOrientation(),
        range: {
            'min': 0,
            'max': 1000
        }
    })
    A.r4 = noUiSlider.create(r4, {
        start: 0,
        connect: [true, false],
        orientation: setOrientation(),
        range: {
            'min': 0,
            'max': 100
        }
    })
    A.r5 = noUiSlider.create(r5, {
        start: 0,
        connect: [true, false],
        orientation: setOrientation(),
        step: 1,
        range: {
            'min': 1,
            'max': 7
        }
    })
}