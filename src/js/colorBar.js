import {mapRange} from './util/util.js';
import {heartParts} from './globals';



export default class colorBar {
    constructor() {
        this.canvas = document.getElementById('colorBar');
        this.context = this.canvas.getContext('2d');
        this.img = new Image();
        this.img.src = 'assets/imgs/color-bar.png';
        this.height = 803;
        this.width = 1;
        this.canvas.height = 803;
        this.canvas.width = 1;

    };
    
    setup() {
        this.img.onload = () => {
            this.context.clearRect(0, 0, this.width, this.height);
            this.context.drawImage(this.img, 0, 0, this.width, this.height);
        }
        
    };

    update(y){
        let finalY =  parseInt(mapRange(y, 1, 1000, 1, this.height));
        let colors = [];
        let px = this.context.getImageData(0, finalY, 1, 1).data;
        colors.push(px[0], px[1], px[2]);
        let finalColors = [];
        colors.forEach((el, i, arr) => {
            let temp =  mapRange(el, 0, 255, 0.0, 1.0);
            finalColors.push(temp);
        })

        return [finalColors[0], finalColors[1], finalColors[2]];

  
    }
}

