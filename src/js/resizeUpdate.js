import throttle  from 'lodash/throttle';
import debounce  from 'lodash/debounce';
import {dpi, composer} from './globals';
import * as Rotator from './3d/rotation';

let wW,
    wH;
const aspectCalc = () => {
  let min = Math.min(wW, wH);

};

export default function resizeUpdate( camera, renderer) {
  $(window).on('resize', throttle(() => {
    Rotator.init();
    wW = window.innerWidth;
    wH = window.innerHeight;
    renderer.setSize(wW, wH);
    composer.setSize(wW*dpi, wH*dpi);
    camera.aspect = wW/wH;
    camera.updateProjectionMatrix();
  }, 5));
}