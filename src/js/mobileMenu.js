import * as A from './globals';
import enquire from 'enquire.js';

export default function MobileMenu() {
    
    function mobileToggle(e) {
        $('#main-nav, #mobile-toggle').toggleClass('open');
        if ($('#main-nav').hasClass('open')) {
            $('body').css('overflow', 'hidden');
            $('#main-nav').delay(200).css('height', '100vh');
        } else  {
            $('body').css('overflow', 'auto');
            $('#main-nav').delay(200).css('height', '1px');
        }
    }
    function closeMenu(e) {
        setTimeout( () => {
            e.preventDefault();
            $('body').css('overflow', 'auto');
            $('#main-nav').removeClass('open').delay(200).css('height', '1px');
            $('#mobile-toggle').removeClass('open');
        }, 200)
    }

    function setEvents() {
        A.isMobile = true;
        $('#main-nav').css('height', '1px');
        $('#mobile-toggle').on('touchstart', mobileToggle);
        $('#main-nav li a').on('touchstart', closeMenu);
        $('#heart').append('<div class="mobile-dim"></div>')
        $('div.mobile-dim').css({
            'position': 'absolute',
            'top': '0',
            'background': 'rgba(0,0,0,0.1)',
            'left': '0',
            'width': '100vw',
            'height': '100vh',
            'z-index': '5'
        })

    }
    function removeEvents() {
        A.isMobile = false;
        $('#mobile-toggle').off('touchstart', mobileToggle);
        $('#main-nav li a').off('touchstart', closeMenu )
    }

    enquire.register("screen and (max-width:767px)", {
        match: () => {
            setEvents();
            A.isMobile = true
        },
        unmatch: () => { 
            removeEvents();
            A.isMobile = false
        }
    });
}