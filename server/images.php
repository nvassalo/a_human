<?php
    header('Access-Control-Allow-Origin: *', false);
    //header('Access-Control-Allow-Headers: Content-Type', false);


	// requires php5
    define('UPLOAD_DIR', './images/');
    define('DOWNLOAD_DIR', './assets/');

    header('Content-Type: image/png', false);

    $id = uniqid('heart_');

    $filename = $id . '.png';

    try{
        
        $heartSize = 2100;
        $baseSize = 3150;

        $success = move_uploaded_file($_FILES['img']['tmp_name'], UPLOAD_DIR . $filename);
        
        $output = imagecreatefrompng(UPLOAD_DIR . $filename );
        imagesavealpha($output, true);
        
        $black_base = imagecreatefrompng(DOWNLOAD_DIR . 'shirt_type_black.png');
        imagesavealpha($black_base, true);
        imagecopyresampled($black_base, $output, 600, 0, 0, 500, $heartSize , 1600, $baseSize, 2400);
        imagepng($black_base, UPLOAD_DIR . 'black_' . $filename, 7);
        
        $cwh = 3150;
        $nwh = 400;

        $thumbB = imagecreatetruecolor($nwh, $nwh);
        setTransparency($thumbB, $nwh, $nwh);
        imagefilledrectangle($thumbB, 0, 0 , $nwh, $nwh, $transparent);

        imagecopyresampled($thumbB, $black_base, 0, 0, 0, 0, $nwh, $nwh, $cwh, $cwh);
        imagepng($thumbB, UPLOAD_DIR . 'thumb_black_' . $filename, 7);

        imagedestroy($thumbB);
        imagedestroy($black_base);

        
        $white_base = imagecreatefrompng( DOWNLOAD_DIR . 'shirt_type_white.png');
        imagesavealpha($white_base, true);

        $output_white = imagecopyresampled($white_base, $output, 600, 0, 0, 500, $heartSize , 1600, $baseSize, 2400);

        imagepng($white_base, UPLOAD_DIR . 'white_' . $filename, 7);

        $thumbW = imagecreatetruecolor($nwh, $nwh);
        setTransparency($thumbW, $nwh, $nwh);

        imagecopyresampled($thumbW, $white_base, 0, 0, 0, 0, $nwh, $nwh, $cwh, $cwh);
        
        imagepng($thumbW, UPLOAD_DIR . 'thumb_white_' . $filename, 7);

        imagedestroy($thumbW);
        imagedestroy($output);
        imagedestroy($white_base);

        if(!file_exists(UPLOAD_DIR . 'white_' . $filename)
        || !file_exists(UPLOAD_DIR . 'thumb_white_' . $filename)
        || !file_exists(UPLOAD_DIR . 'black_' . $filename)
        || !file_exists(UPLOAD_DIR . 'thumb_black_' . $filename)){
            throw new Exception("Error Processing Request", 1);
        }
        echo $id;
    }
    catch(Exception $e){
        throw $e;
    }


    function setTransparency($cImg, $w, $h)
    {
        imagealphablending($cImg, false);
        imagesavealpha($cImg, true);
        $transparent = imagecolorallocatealpha($cImg, 255,255,255,127);
        imagefilledrectangle($cImg, 0, 0 , $h, $w, $transparent);
    } 
?>