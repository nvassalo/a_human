<?php header('Access-Control-Allow-Origin: *', false);

use Printful\Exceptions\PrintfulApiException;
use Printful\Exceptions\PrintfulException;
use Printful\PrintfulApiClient;
use Printful\PrintfulMockupGenerator;
use Printful\Structures\Generator\MockupGenerationParameters;

require_once __DIR__ . '/vendor/autoload.php';

// Replace this with your API key
$apiKey = 'jrqjg4sg-4u07-q5l7:3tr3-mn2a4viibv19';

$pf = new PrintfulApiClient($apiKey);
$mockup = new PrintfulMockupGenerator($pf);

try {

     $paras = new MockupGenerationParameters();
     $paras->$productId = 71;
     $paras->$variant_ids = [1320];
     $paras->$format = FORMAT_PNG;
     print_r(new MockupGenerationParameters());
     /* array(
        productId => 71,
        variant_ids => 1320,
        format => 'png',
        files => array(
                placement => 'default',
                image_url => 'http://ahumanbody.com/server/images/shirt.png',
                position => array(
                    area_width => 520, // Value of print_area_width in the template
                    area_height => 202, // Value of print_area_height in the template
                    width => 140, // Image width
                    height => 63, // Image height
                    top => 77, // Image top offset in area
                    left => 43 // Image left offset in area
            )
        )       
    ); */

    $files = $mockup->createGenerationTaskAndWaitForResult($paras);
    print_r($files);
    

    // -------------------------------

    // -------------------------------
    // Create an order and confirm immediately
    /*
     $order = $pf->post('orders',
        [
            'recipient' => [
                'name' => 'John Doe',
                'address1' => '172 W Street Ave #105',
                'city' => 'Burbank',
                'state_code' => 'CA',
                'country_code' => 'US',
                'zip' => '91502',
            ],
            'items' => [
                [
                    'variant_id' => 1,// Small poster
                    'name' => 'Niagara Falls poster', // Display name
                    'retail_price' => '19.99', // Retail price for packing slip
                    'quantity' => 1,
                    'files' => [
                        [
                            'url' => 'http://example.com/files/posters/poster_1.jpg',
                        ],
                    ],
                ],
            ],
        ],
        ['confirm' => 1]
    );
    var_export($order);
    */
    // -------------------------------

} catch (PrintfulApiException $e) {
    // API response status code was not successful
    echo 'Printful API Exception: ' . $e->getCode() . ' ' . $e->getMessage();
} catch (PrintfulException $e) {
    // API call failed
    echo 'Printful Exception: ' . $e->getMessage();
    var_export($pf->getLastResponseRaw());
}
