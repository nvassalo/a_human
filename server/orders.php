<?php 
header('Access-Control-Allow-Origin: *', false);
header('Access-Control-Allow-Headers: Content-Type', false);
header('Content-Type: application/x-www-form-urlencoded');

// Create an order
    $x = file_get_contents('php://input');
    $data = json_decode($x, true);

    echo 'As it comes: '. $data;

    $name = $data['name'];
    $address = $data['address'];
    $country_code = $data['country'];
    $city = $data['city'];
    $state = $data['state'];
    $zip = $data['zip'];
    $email = $data['email'];
    $image_file = $data['image'];
    $model = $data['model'];

    echo "Isolada: " . $model;

$url = "https://api.printful.com/orders?confirm=1";

//model in fields_string too
$fields_string = "{\n    \"recipient\": {\n        \"name\": \"".$name."\",\n        \"address1\": \"".$address."\",\n        \"city\": \"".$city."\",\n        \"country_code\":\"".$country_code."\",\n        \"state_code\": \"".$state."\",\n        \"zip\": \"".$zip."\",\n        \"email\": \"".$email."\"\n    },\n    \"items\": [{\n        \"variant_id\": \"".$model."\",\n        \"quantity\": 1,\n        \"retail_price\": \"40.00\",\n        \"name\":\"A. Human tee\",\n        \"files\": [{\n            \"url\": \"".$image_file."\"\n        }]\n    }],\n    \"confirm\":1\n}";
echo "json: ".$fields_string;
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => $fields_string,
  CURLOPT_HTTPHEADER => array(
    "Authorization: Basic " . base64_encode("jrqjg4sg-4u07-q5l7:3tr3-mn2a4viibv19"),
    "Cache-Control: no-cache",
    "Content-Type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}